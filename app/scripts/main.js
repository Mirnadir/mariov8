$(document).ready(function(){
    // homepage slider [[
    $('.c-home-content__slider .owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        smartSpeed: 450,
        dots: true,
        nav: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });
    // homepage slider ]]
    
    // innerpage slider [[
    $('.c-innerPage-content__slider .owl-carousel').owlCarousel({
        loop: true,
        autoplay: true,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        smartSpeed: 450,
        dots: false,
        nav: true,
        responsive: {
            0: {
                items: 1
            }
        }
    });
    // innerpage slider ]]
    
    // filter --- [[
    $('#js-filter__all-games li a').click(function() {
        var filterClass = $(this).attr('class');

        $('#js-filter__all-games li').removeClass('active');
        $(this).parent().addClass('active');
        
        $('#js-tab-holder__all-games').children('div:not(.' + filterClass + ')').hide();
        $('#js-tab-holder__all-games').children('div.' + filterClass).show();
    });
    // filter --- ]]

    // mobile dropdown menu --- [[
    $('.dropdown').on('show.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideDown(200);
        $('.dropdown-toggle').css('box-shadow', '0px -10px 20px -5px rgba(211, 211, 211, 0.5)')
    });
      
    $('.dropdown').on('hide.bs.dropdown', function(e){
        $(this).find('.dropdown-menu').first().stop(true, true).slideUp(100);
        $('.dropdown-toggle').css('box-shadow', '0 0 20px 0 rgba(211, 211, 211, 0.5)')
    });
      // mobile dropdown menu --- ]]

      
  });
